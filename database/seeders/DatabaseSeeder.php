<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        //
        //\App\Models\Perfil::create([
        DB::table('perfil')->insert([ //{{{
            [
                'codigo' => 'ADMIN',
                'nombre' => 'Administradores',
            ],
            [
                'codigo' => 'REGIS',
                'nombre' => 'Registrador',
            ],
            [
                'codigo' => 'VERIF',
                'nombre' => 'Verificador',
            ],
        ]); //}}}

        //\App\Models\User::create([
        DB::table('users')->insert([ //{{{
            [
                'name' => 'theadmin',
                'email' => 'theadmin@localhost',
                'perfil' => 'ADMIN',
                'password' => Hash::make('theadmin_321__'),
            ],
            [
                'name' => 'registrador1',
                'email' => 'registrador1@localhost',
                'perfil' => 'ADMIN',
                'password' => Hash::make('registrador2_321__'),
            ],
            [
                'name' => 'registrador2',
                'email' => 'registrador2@localhost',
                'perfil' => 'ADMIN',
                'password' => Hash::make('registrador1_321__'),
            ],
            [
                'name' => 'verificador2',
                'email' => 'verificador2@localhost',
                'perfil' => 'VERIF',
                'password' => Hash::make('verificador_321__'),
            ],
        ]); //}}}

        DB::table('jug_tipo_contacto')->insert([
            ['nombre' => 'Whatsapp'],
            ['nombre' => 'Telegram'],
        ]);

        DB::table('gen_banco')->insert([
            ['nombre' => 'Banco de la nación'],
            ['nombre' => 'Interbank'],
            ['nombre' => 'BBVA'],
            ['nombre' => 'BCP'],
            ['nombre' => 'Scotiabank'],
        ]);

        DB::table('par_partido')->insert([
            [
                'id' => 1,
                'nombre' => 'Peru - Chile',
                'hora_inicio_partido' => '2022-12-01 00:00:00',
                'hora_fin_partido' => '2023-12-01 23:59:59',
                'hora_inicio_apuesta' => '2022-12-01 00:00:00',
                'hora_fin_apuesta' => '2023-02-01 23:59:59',
            ],
            [
                'id' => 2,
                'nombre' => 'Peru - Argentina',
                'hora_inicio_partido' => '2022-12-01 00:00:00',
                'hora_fin_partido' => '2023-12-01 23:59:59',
                'hora_inicio_apuesta' => '2022-12-01 00:00:00',
                'hora_fin_apuesta' => '2023-02-01 23:59:59',
            ],
            [
                'id' => 3,
                'nombre' => 'Peru - Colombia',
                'hora_inicio_partido' => '2022-12-01 00:00:00',
                'hora_fin_partido' => '2022-12-11 23:59:59',
                'hora_inicio_apuesta' => '2022-12-01 00:00:00',
                'hora_fin_apuesta' => '2023-12-11 23:59:59',
            ],
        ]);
    }
}
