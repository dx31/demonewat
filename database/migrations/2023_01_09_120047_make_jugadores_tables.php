<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('jug_jugador', function (Blueprint $table) {
            $table->id();
            //$table->char('id_jugador', 9)->unique();
            $table->string('nombres');
            $table->timestamps();
            $table->decimal('saldo_cuenta', 19, 2)->unsigned();
        });


        Schema::create('jug_tipo_contacto', function(Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->timestamps();
        });

        Schema::create('jug_contacto', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('jugador')->unsigned();
            $table->bigInteger('tipo_contacto')->unsigned();
            $table->string('numero_contacto');
            $table->timestamps();

            $table->foreign('jugador')->references('id')->on('jug_jugador');
            $table->foreign('tipo_contacto')->references('id')->on('jug_tipo_contacto');

            $table->unique(['jugador', 'tipo_contacto'], 'ux_jugc_jt');
        });

        Schema::create('jug_cuenta_bancaria', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('jugador')->unsigned();
            $table->bigInteger('banco')->unsigned();
            $table->string('numero_cuenta', 25);
            $table->string('numero_cci', 25);
            $table->timestamps();

            $table->foreign('jugador', 'fk_jugcb_jugj')->references('id')->on('jug_jugador');
            $table->foreign('banco', 'fk_jugcb_genb')->references('id')->on('gen_banco');

            $table->unique(['jugador', 'banco', 'numero_cuenta'], 'uq_jugcb_1');
            $table->unique(['jugador', 'banco', 'numero_cci'], 'uq_jugcb_2');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
