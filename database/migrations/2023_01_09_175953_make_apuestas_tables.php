<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apt_apuesta', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('jugador')->unsigned();
            $table->decimal('monto_apostado', 16, 4)->unsigned();
            $table->timestamps();
        });

        //_pv: por verificar
        Schema::create('apt_pago_pv', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('apuesta')->unsigned();
            $table->bigInteger('cuenta_bancaria')->unsigned();
            $table->decimal('deposito', 16, 4)->unsigned();
            $table->timestamps();
        });

        Schema::create('apt_pago', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('apuesta')->unsigned();
            $table->bigInteger('cuenta_bancaria')->unsigned();
            $table->decimal('deposito', 16, 4)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
