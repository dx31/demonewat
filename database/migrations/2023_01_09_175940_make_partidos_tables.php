<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('par_partido', function(Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->dateTime('hora_inicio_partido');
            $table->dateTime('hora_fin_partido');
            $table->dateTime('hora_inicio_apuesta');
            $table->dateTime('hora_fin_apuesta');
            $table->timestamps();
        });

        Schema::create('par_pago', function(Blueprint $table) {
            $table->id();
            $table->bigInteger('partido')->unsigned();
            $table->string('nombre_pago');
            $table->timestamps();

            $table->foreign('partido')->references('id')->on('par_partido');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
