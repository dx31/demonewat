<?php
namespace App\Services;
use App\Http\Requests\ApuestaRequest;
use Illuminate\Support\Facades\DB;
use \App\Models\JugadorModel;
use \App\Models\ApuestaModel;
use \App\Models\ContactoModel;
use \App\Models\CuentaBancariaModel;

class ApuestaService {
    public function store(ApuestaRequest $request) {
        $validated = $request->validated();

        return DB::transaction(function() use($validated) {
            $jugador = \App\Models\JugadorModel::findOrFail($validated['jugador']);
            $tipo_contacto = \App\Models\ContactoModel::findOrFail($validated['numero_contacto']);
            $cuenta_bancaria = \App\Models\CuentaBancariaModel::findOrFail($validated['numero_cuenta']);

            if($jugador['id'] !== $tipo_contacto['jugador'] ||
                $jugador['id'] !== $cuenta_bancaria['jugador']) {
                throw new \Exception('La cuenta y/o número de contacto no corresponde al jugador');
            }

            $apuesta = ApuestaModel::create([
                'jugador' => $jugador->id,
                'monto_apostado' => $validated['monto'],
            ]); 

            $jugador->update(['saldo_cuenta' => $jugador['saldo_cuenta'] + $validated['monto'] ]);

            return $apuesta;
        });
    }

    public function listarMovimientos($id) {
        return ApuestaModel::where('jugador', $id)->get();
    }

    public function eliminarApuesta($id) {
        DB::transaction(function() use($id) {
            $apuesta = ApuestaModel::findOrFail($id);
            $jugador = JugadorModel::findOrFail($apuesta['jugador']);

            if(($jugador['saldo_cuenta'] - $apuesta['monto_apostado'])<0)
                throw new \Exception('Saldo negativo');

            $jugador->update(['saldo_cuenta' => $jugador['saldo_cuenta'] - $apuesta['monto_apostado'] ]);

            $apuesta->delete();
        });

        return true;
    }
}
