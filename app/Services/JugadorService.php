<?php
namespace App\Services;
use App\Http\Requests\JugadorRequest;
use Illuminate\Support\Facades\DB;
use \App\Models\JugadorModel;
use \App\Models\ApuestaModel;
use \App\Models\ContactoModel;
use \App\Models\CuentaBancariaModel;

class JugadorService {
    public function store(JugadorRequest $request) {
        $validated = $request->validated();

        return DB::transaction(function() use($validated) {
            $validated['saldo_cuenta'] = 0;
            $jugador = \App\Models\JugadorModel::create($validated);

            CuentaBancariaModel::updateOrCreate([
                'jugador' => $jugador['id'],
                'banco' => $validated['banco'],
                'numero_cuenta' => $validated['numero_cuenta'],
                'numero_cci' => '',
            ]);

            ContactoModel::updateOrCreate([
                'jugador' => $jugador['id'],
                'tipo_contacto' => $validated['tipo_contacto'],
                'numero_contacto' => $validated['numero_contacto'],
            ]);
                                    

            if($validated['monto'] ?? false) {
                $apuesta = ApuestaModel::create([
                    'jugador' => $jugador->id,
                    'monto_apostado' => $validated['monto'],
                ]); 
                $jugador->update(['saldo_cuenta' => $jugador['saldo_cuenta'] + $validated['monto'] ]);
            }

            return $jugador;
        });
    }
}
