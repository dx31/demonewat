<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ParPartido
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $hora_inicio_partido
 * @property Carbon $hora_fin_partido
 * @property Carbon $hora_inicio_apuesta
 * @property Carbon $hora_fin_apuesta
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|ParPago[] $par_pagos
 *
 * @package App\Models
 */
class ParPartido extends Model
{
	protected $table = 'par_partido';

	protected $dates = [
		'hora_inicio_partido',
		'hora_fin_partido',
		'hora_inicio_apuesta',
		'hora_fin_apuesta'
	];

	protected $fillable = [
		'nombre',
		'hora_inicio_partido',
		'hora_fin_partido',
		'hora_inicio_apuesta',
		'hora_fin_apuesta'
	];

	public function par_pagos()
	{
		return $this->hasMany(ParPago::class, 'partido');
	}
}
