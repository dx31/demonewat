<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoContactoModel;

/**
 * Class JugComunicacion
 * 
 * @property int $id
 * @property int $jugador
 * @property int $tipo_comunicacion
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property JugJugador $jug_jugador
 * @property JugTipoComunicacion $jug_tipo_comunicacion
 *
 * @package App\Models
 */
class ContactoModel extends Model
{
	protected $table = 'jug_contacto';

	protected $casts = [
		'jugador' => 'int',
		'tipo_contacto' => 'int'
	];

	protected $fillable = [
		'jugador',
		'tipo_contacto',
		'numero_contacto'
	];

	public function jugador_()
	{
		return $this->belongsTo(JugadorModel::class, 'jugador');
	}

	public function tipoContacto_()
	{
		return $this->belongsTo(TipoContactoModel::class, 'tipo_contacto');
	}
}
