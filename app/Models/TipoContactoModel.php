<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JugTipoComunicacion
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|JugComunicacion[] $jug_comunicacions
 *
 * @package App\Models
 */
class TipoContactoModel extends Model
{
	protected $table = 'jug_tipo_contacto';

	protected $fillable = [
		'nombre'
	];
}
