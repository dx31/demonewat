<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\ApuestaModel;
use App\Models\ContactoModel;
use App\Models\CuentaBancariaModel;

/**
 * Class Jugador
 * 
 * @property int $id
 * @property string $id_jugador
 * @property string $nombres
 * @property string $apellido_paterno
 * @property string|null $apellido_materno
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|JugComunicacion[] $jug_comunicacions
 * @property Collection|JugCuentaBancarium[] $jug_cuenta_bancaria
 *
 * @package App\Models
 */
class JugadorModel extends Model
{
	protected $table = 'jug_jugador';

	protected $fillable = [
		'id_jugador',
		'nombres',
		'saldo_cuenta',
	];

	public function contactos_()
	{
		return $this->hasMany(ContactoModel::class, 'jugador');
	}

	public function cuentasBancarias_()
	{
		return $this->hasMany(CuentaBancariaModel::class, 'jugador');
	}

	public function apuestas_()
	{
		return $this->hasMany(Apuesta::class, 'jugador');
	}
}
