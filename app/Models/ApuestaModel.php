<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\JugadorModel;

/**
 * Class AptApuestum
 * 
 * @property int $id
 * @property int $jugador
 * @property float $monto_apostado
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ApuestaModel extends Model
{
	protected $table = 'apt_apuesta';

	protected $casts = [
		'jugador' => 'int',
		'monto_apostado' => 'float'
	];

	protected $fillable = [
		'jugador',
		'monto_apostado'
	];

	public function jugador_()
	{
		return $this->belongsTo(JugadorModel::class, 'jugador');
	}
}
