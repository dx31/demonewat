<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JugCuentaBancarium
 * 
 * @property int $id
 * @property int $jugador
 * @property int $banco
 * @property string $numero_cuenta
 * @property string $numero_cci
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property GenBanco $gen_banco
 * @property JugJugador $jug_jugador
 *
 * @package App\Models
 */
class CuentaBancariaModel extends Model
{
	protected $table = 'jug_cuenta_bancaria';

	protected $casts = [
		'jugador' => 'int',
		'banco' => 'int'
	];

	protected $fillable = [
		'jugador',
		'banco',
		'numero_cuenta',
		'numero_cci'
	];

	public function banco_()
	{
		return $this->belongsTo(BancoModel::class, 'banco');
	}

	public function jugador_()
	{
		return $this->belongsTo(JugadorModel::class, 'jugador');
	}
}
