<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AptPago
 * 
 * @property int $id
 * @property int $apuesta
 * @property int $cuenta_bancaria
 * @property float $deposito
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AptPago extends Model
{
	protected $table = 'apt_pago';

	protected $casts = [
		'apuesta' => 'int',
		'cuenta_bancaria' => 'int',
		'deposito' => 'float'
	];

	protected $fillable = [
		'apuesta',
		'cuenta_bancaria',
		'deposito'
	];
}
