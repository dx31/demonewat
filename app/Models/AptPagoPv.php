<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AptPagoPv
 * 
 * @property int $id
 * @property int $apuesta
 * @property int $cuenta_bancaria
 * @property float $deposito
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AptPagoPv extends Model
{
	protected $table = 'apt_pago_pv';

	protected $casts = [
		'apuesta' => 'int',
		'cuenta_bancaria' => 'int',
		'deposito' => 'float'
	];

	protected $fillable = [
		'apuesta',
		'cuenta_bancaria',
		'deposito'
	];
}
