<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ParPago
 * 
 * @property int $id
 * @property int $partido
 * @property string $nombre_pago
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property ParPartido $par_partido
 *
 * @package App\Models
 */
class ParPago extends Model
{
	protected $table = 'par_pago';

	protected $casts = [
		'partido' => 'int'
	];

	protected $fillable = [
		'partido',
		'nombre_pago'
	];

	public function par_partido()
	{
		return $this->belongsTo(ParPartido::class, 'partido');
	}
}
