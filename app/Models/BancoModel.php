<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GenBanco
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|JugCuentaBancarium[] $jug_cuenta_bancaria
 *
 * @package App\Models
 */
class BancoModel extends Model
{
	protected $table = 'gen_banco';

	protected $fillable = [
		'nombre'
	];

}
