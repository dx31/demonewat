<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CuentaBancariaResource extends JsonResource {
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'banco' => $this->banco,
            'banco_nombre' => $this->banco_->nombre,
            'numero_cuenta' => $this->numero_cuenta,
        ];
    }
}

class ContactoResource extends JsonResource {
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_contacto' => $this->tipo_contacto,
            'tipo_contacto_nombre' => $this->tipoContacto_->nombre,
            'numero_contacto' => $this->numero_contacto,
        ];
    }
}

class JugadorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombres' => $this->nombres,
            'saldo_cuenta' => $this->saldo_cuenta,
            'contactos_' => ContactoResource::collection($this->contactos_),
            'cuentas_bancarias_' => CuentaBancariaResource::collection($this->cuentasBancarias_),
        ];
    }
}
