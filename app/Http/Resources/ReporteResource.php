<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReporteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $retval = [
            'id' => $this->id,
            'nombres' => $this->jugador_->nombres,
            'monto' => $this->monto_apostado,
            'fecha' => $this->created_at,
        ];

        if($this->jugador_ && $this->jugador_->contactos_) {
            $retval['numero_contacto'] = $this->jugador_->contactos_[0]->numero_contacto;
            $retval['tipo_contacto'] =   $this->jugador_->contactos_[0]->tipoContacto_->nombre;
        }

        if($this->jugador_ && $this->jugador_->cuentasBancarias_) {
            $retval['numero_contacto'] = $this->jugador_->cuentasBancarias_[0]->numero_cuenta;
            $retval['tipo_contacto'] =   $this->jugador_->cuentasBancarias_[0]->banco_->nombre;
        }

        return $retval;
    }
}
