<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ApuestaRequest;
use App\Http\Resources\TipoContactoResource;
use App\Http\Resources\BancoResource;
use App\Http\Resources\JugadorResource;
use App\Http\Resources\MovimientoResource;
use App\Http\Resources\ReporteResource;
use App\Models\TipoContactoModel;
use App\Models\BancoModel;
use App\Models\JugadorModel;
use App\Models\ApuestaModel;
use Facades\App\Services\ApuestaService;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apuestas = ApuestaModel::with('jugador_.contactos_')
                        ->with('jugador_.cuentasBancarias_')
                        ->get()
                        ->map(function($x) {
                            $retval = [
                                'id' => $x->id,
                                'nombres' => $x->jugador_->nombres,
                                'monto' => $x->monto_apostado,
                                'fecha' => $x->created_at,
                            ];

                            if($x->jugador_ && $x->jugador_->contactos_) {
                                $retval['numero_contacto'] = $x->jugador_->contactos_[0]->numero_contacto;
                                $retval['tipo_contacto'] =   $x->jugador_->contactos_[0]->tipoContacto_->nombre;
                            }

                            if($x->jugador_ && $x->jugador_->cuentasBancarias_) {
                                $retval['numero_cuenta'] = $x->jugador_->cuentasBancarias_[0]->numero_cuenta;
                                $retval['banco'] =   $x->jugador_->cuentasBancarias_[0]->banco_->nombre;
                            }


                            return $retval;

                        });


        return view('reportes.index', [
            'items'=>($apuestas),
        ]);
    }

    /*
    public function store(ApuestaRequest $request) {
        $apuesta = ApuestaService::store($request);

        return redirect(route('apuestas.index'))->with([
            ['message'=> 'Registro creado']
        ]);
    }

    public function listar($id) {
        return MovimientoResource::collection(
            ApuestaService::listarMovimientos($id)
        );
    }

    public function anularApuesta($id) {
        $retval = ApuestaService::eliminarApuesta($id);
        return response()->json(['ok'=> 0]);
    }
     */
}
