<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\JugadorRequest;
use App\Http\Resources\JugadorResource;
use App\Models\JugadorModel;
use Facades\App\Services\JugadorService;

class JugadorController extends Controller
{
    //
    //
    public function store(JugadorRequest $request) {
        $jugador = JugadorService::store($request);

        return redirect(route('apuestas.index'))->with([
            ['message'=> 'Registro creado']
        ]);
    }

    public function show($id) {
        return new JugadorResource(JugadorModel
                        ::with('cuentasBancarias_')
                        ->with('contactos_')
                        ->findOrFail($id)
                );
    }
}
