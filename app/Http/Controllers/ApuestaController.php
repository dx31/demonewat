<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ApuestaRequest;
use App\Http\Resources\TipoContactoResource;
use App\Http\Resources\BancoResource;
use App\Http\Resources\JugadorResource;
use App\Http\Resources\MovimientoResource;
use App\Models\TipoContactoModel;
use App\Models\BancoModel;
use App\Models\JugadorModel;
use Facades\App\Services\ApuestaService;

class ApuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('apuestas.index', [
            'items'=>JugadorResource::collection(JugadorModel::all()),
            'tipos_contacto'=> TipoContactoResource::collection(TipoContactoModel::all()),
            'bancos' => BancoResource::collection(BancoModel::all()),
        ]);
    }

    public function store(ApuestaRequest $request) {
        $apuesta = ApuestaService::store($request);

        return redirect(route('apuestas.index'))->with([
            ['message'=> 'Registro creado']
        ]);
    }

    public function listar($id) {
        return MovimientoResource::collection(
            ApuestaService::listarMovimientos($id)
        );
    }

    public function anularApuesta($id) {
        $retval = ApuestaService::eliminarApuesta($id);
        return response()->json(['ok'=> 0]);
    }
}
