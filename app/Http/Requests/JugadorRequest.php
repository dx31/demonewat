<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JugadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nombres' => ['required', 'string', 'max:255'],
            'banco' => ['required', 'numeric', 'exists:gen_banco,id'],
            'numero_cuenta' => ['required', 'string', 'max:25'],
            'tipo_contacto' => ['required', 'numeric', 'exists:jug_tipo_contacto,id'],
            'numero_contacto' => ['required', 'string', 'max:25'],
            'monto' => ['required', 'numeric', 'min:0'],
            //
        ];
    }
}
