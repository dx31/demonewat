<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApuestaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'jugador' => ['required', 'numeric', 'exists:jug_jugador,id'],
            'numero_cuenta' => ['required', 'numeric', 'exists:jug_cuenta_bancaria,id'],
            'numero_contacto' => ['required', 'numeric', 'exists:jug_contacto,id'],
            'monto' => ['required', 'numeric', 'min:1'],
            //
        ];
    }
}
