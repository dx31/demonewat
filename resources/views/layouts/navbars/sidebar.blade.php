<div class="sidebar" data-image="{{ asset('light-bootstrap/img/sidebar-5.jpg') }}">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                {{ __("Creative Tim") }}
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item @if($activePage == 'apuestas') active @endif">
                <a class="nav-link" href="{{route('apuestas.index')}}">
                    <i class="nc-icon nc-notes"></i>
                    <p>{{ __("Registrar apuesta") }}</p>
                </a>
            </li>
            <li class="nav-item @if($activePage == 'reportes') active @endif">
                <a class="nav-link" href="{{route('reportes.index')}}">
                    <i class="nc-icon nc-notes"></i>
                    <p>{{ __("Reportes") }}</p>
                </a>
            </li>

        </ul>
    </div>
</div>
