@extends('layouts.app', ['activePage' => 'reportes', 'title' => 'Reportes', 'navName' => 'Reportes', 'activeButton' => 'laravel'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card strpied-tabled-with-hover">
                        <div class="card-header ">
                            <h4 class="card-title">Apuestas registradas</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body table-full-width table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <th>Fecha</th>
                                    <th>Jugador</th>
                                    <th>Monto</th>
                                    <th>Contacto</th>
                                    <th>Cuenta</th>
                                </thead>
                                <tbody>
                                    @foreach($items as $x)
                                    <tr>
                                        <td>{{$x['fecha'] }}</td>
                                        <td>{{$x['nombres'] }}</td>
                                        <td>{{$x['monto'] }}</td>
                                        <td>{{$x['numero_contacto']}}</td>
                                        <td>{{$x['numero_cuenta']}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('postjs')
    <script>
        $(document).ready(() => {
            $('#modal-registro-show').click(() => {
                $('#modal-registro').modal('show');
            });

            $('#btn-registrar-jugador').click(() => {
                $.ajax({
                    method: 'POST',
                    dataType: 'json',                    
                    data: {
                        nombre: $('#registro-nombres').text(),
                        banco: $('#registro-banco').text(),
                        numero_cuenta: $('#registro-numero-cuenta').text(),
                        tipo_contacto: $('#registro-tipo-contacto').text(),
                        numero_contacto: $('#registro-numero-contacto').text(),
                        monto: $('#registro-monto').text() || null,
                    }
                });    
            });

            $(document).on('click', '.recarga-saldo', (e) => {
                let tr = $(e.target).closest('tr');
                let id = $(e.target).closest('tr').data('id');
                if(! id) throw 'No id';

                $.get(`{{route('api_jugadores.show', '')}}/${id}`, (response) => {
                    const md = $('#modal-recarga');
                    $('#modal-recarga .recarga-id').val(id);
                    $('#modal-recarga .recarga-nombres').val(response.data.nombres);
                    $('#modal-recarga .recarga-saldo').val(response.data.saldo_cuenta);
                    $('#modal-recarga .recarga-cuenta-bancaria').html('');
                    (response.data.cuentas_bancarias_ || []).forEach(x => {
                        $('#modal-recarga .recarga-cuenta-bancaria')
                            .append(`<option value="${x.id}">${x.numero_cuenta}</option>`);
                    });
                    $('#modal-recarga .recarga-contacto').html('');
                    (response.data.contactos_ || []).forEach(x => {
                        $('#modal-recarga .recarga-contacto')
                            .append(`<option value="${x.id}">${x.numero_contacto} (${x.tipo_contacto_nombre})</option>`);
                    });

                    $(md).modal('show');
                    
                });

            });

            $(document).on('click', '.ver-movimientos', (e) => {
                let tr = $(e.target).closest('tr');
                let id = $(e.target).closest('tr').data('id');
                if(! id) throw 'No id';

                $.get(`{{route('apuestas.lista', '')}}/${id}`, (response) => {
                    const md = $('#modal-movimientos');
                    $('#modal-movimientos .recarga-id').val(id);
                    $('#modal-movimientos .recarga-nombres').val(response.data.nombres);
                    $('#modal-movimientos .tabla-movimientos').html('');
                    (response.data || []).forEach(x => {
                        $('#modal-movimientos .tabla-movimientos')
                            .append(`<tr data-id="${x.id}">
                                        <td>${x.fecha}</td>
                                        <td>${x.monto}</td>
                                        <td><button class="btn anular-movimiento">Anular</button></td>
                                    </tr>`);
                    });

                    $(md).modal('show');
                    
                });

            });

            $(document).on('click', '.anular-movimiento', (e) => {
                if(!window.confirm('Anular este movimiento')) return;

                const tr = $(e.target).closest('tr');
                const id = $(tr).data('id');

                $.ajax(`{{ route('anular.movimiento', '')}}/${id}`, {
                    method: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                    }
                }).done(() => {
                    window.location.reload(false);
                });

            });
        });
    </script>
    @endpush
@endsection
