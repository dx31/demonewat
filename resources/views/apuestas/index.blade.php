@extends('layouts.app', ['activePage' => 'table', 'title' => 'Apuestas', 'navName' => 'Table List', 'activeButton' => 'laravel'])


@section('content')
    <div class="content">
        @include('apuestas.modal-recarga', ['id'=>'modal-recarga'])
        @include('apuestas.modal-movimientos', ['id'=>'modal-movimientos'])
        <div class="modal" id="modal-registro" role="dialog">
            <form action="{{route('jugadores.store') }}" method="post">
                {{ csrf_field() }}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title">Registro de jugador</div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input id="registro-nombres" name="nombres" type="text" class="form-control" placeholder="Nombres"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Banco</label>
                                    <select id="registro-banco" name="banco" class="form-control" placeholder="Banco">
                                        @foreach($bancos as $b)
                                        <option value="{{$b->id}}">{{$b->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="form-group">
                                    <label>Número de cuenta</label>
                                    <input id="registro-numero-cuenta" name="numero_cuenta" type="text" class="form-control" placeholder="Número de cuenta"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Contacto</label>
                                    <select id="registro-tipo-contacto" name="tipo_contacto" class="form-control" placeholder="Tipo">
                                        @foreach($tipos_contacto as $t)
                                        <option value="{{$t->id}}">{{$t->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="form-group">
                                    <label>Número</label>
                                    <input id="registro-numero-contacto" name="numero_contacto" type="text" class="form-control" placeholder="Número"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Monto de apuesta (S/) (Opcional)</label>
                                    <input id="registro-monto" name="monto" type="text" class="form-control" placeholder="Monto"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn-registrar-jugad1or" type="submit" class="btn btn-primary">Registrar</button>
                        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                    </div>
                </div> 
            </div>
            </form>
        </div>

        <div class="container-fluid">
            <div class="row">
                <button class="btn btn-primary" id="modal-registro-show">Registrar jugador</button>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card strpied-tabled-with-hover">
                        <div class="card-header ">
                            <h4 class="card-title">Apuestas registradas</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body table-full-width table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <th>ID</th>
                                    <th>Jugador</th>
                                    <th>Saldo</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach($items as $x)
                                    <tr data-id="{{$x['id']}}">
                                        <td>{{$x['id']}}</td>
                                        <td class="nombres">{{$x['nombres']}}</td>
                                        <td class="saldo-cuenta">{{$x['saldo_cuenta']}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn dropdown-toggle btn-opciones"
                                                    data-toggle="dropdown" 
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                >
                                                    <i class="bi bi-three-dots"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item recarga-saldo" href="#">
                                                        Recargar saldo
                                                    </a>
                                                    <a class="dropdown-item ver-movimientos" href="#">
                                                        Ver movimientos
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('postjs')
    <script>
        $(document).ready(() => {
            $('#modal-registro-show').click(() => {
                $('#modal-registro').modal('show');
            });

            $('#btn-registrar-jugador').click(() => {
                $.ajax({
                    method: 'POST',
                    dataType: 'json',                    
                    data: {
                        nombre: $('#registro-nombres').text(),
                        banco: $('#registro-banco').text(),
                        numero_cuenta: $('#registro-numero-cuenta').text(),
                        tipo_contacto: $('#registro-tipo-contacto').text(),
                        numero_contacto: $('#registro-numero-contacto').text(),
                        monto: $('#registro-monto').text() || null,
                    }
                });    
            });

            $(document).on('click', '.recarga-saldo', (e) => {
                let tr = $(e.target).closest('tr');
                let id = $(e.target).closest('tr').data('id');
                if(! id) throw 'No id';

                $.get(`{{route('api_jugadores.show', '')}}/${id}`, (response) => {
                    const md = $('#modal-recarga');
                    $('#modal-recarga .recarga-id').val(id);
                    $('#modal-recarga .recarga-nombres').val(response.data.nombres);
                    $('#modal-recarga .recarga-saldo').val(response.data.saldo_cuenta);
                    $('#modal-recarga .recarga-cuenta-bancaria').html('');
                    (response.data.cuentas_bancarias_ || []).forEach(x => {
                        $('#modal-recarga .recarga-cuenta-bancaria')
                            .append(`<option value="${x.id}">${x.numero_cuenta}</option>`);
                    });
                    $('#modal-recarga .recarga-contacto').html('');
                    (response.data.contactos_ || []).forEach(x => {
                        $('#modal-recarga .recarga-contacto')
                            .append(`<option value="${x.id}">${x.numero_contacto} (${x.tipo_contacto_nombre})</option>`);
                    });

                    $(md).modal('show');
                    
                });

            });

            $(document).on('click', '.ver-movimientos', (e) => {
                let tr = $(e.target).closest('tr');
                let id = $(e.target).closest('tr').data('id');
                if(! id) throw 'No id';

                $.get(`{{route('apuestas.lista', '')}}/${id}`, (response) => {
                    const md = $('#modal-movimientos');
                    $('#modal-movimientos .recarga-id').val(id);
                    $('#modal-movimientos .recarga-nombres').val(response.data.nombres);
                    $('#modal-movimientos .tabla-movimientos').html('');
                    (response.data || []).forEach(x => {
                        $('#modal-movimientos .tabla-movimientos')
                            .append(`<tr data-id="${x.id}">
                                        <td>${x.fecha}</td>
                                        <td>${x.monto}</td>
                                        <td><button class="btn anular-movimiento">Anular</button></td>
                                    </tr>`);
                    });

                    $(md).modal('show');
                    
                });

            });

            $(document).on('click', '.anular-movimiento', (e) => {
                if(!window.confirm('Anular este movimiento')) return;

                const tr = $(e.target).closest('tr');
                const id = $(tr).data('id');

                $.ajax(`{{ route('anular.movimiento', '')}}/${id}`, {
                    method: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                    }
                }).done(() => {
                    window.location.reload(false);
                });

            });
        });
    </script>
    @endpush
@endsection
