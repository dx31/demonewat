<div class="modal" id="{{$id}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Recarga</div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input readonly type="text" class="form-control recarga-nombres" placeholder="Nombres"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Saldo</label>
                            <input readonly type="text" class="form-control recarga-saldo" placeholder="Saldo"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-body table-full-width table-responsive">
                            <table class="tabla-movimientos table table-hover table-striped">
                                <thead>
                                    <th>Fecha</th>
                                    <th>Monto</th>
                                    <th></th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
            </div>
        </div> 
    </div>
    </form>
</div>
