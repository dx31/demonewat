<div class="modal" id="{{$id}}" role="dialog">
    <form action="{{ route('apuestas.store') }}" method="post">
        {{ csrf_field() }}
        <input name="jugador" class="recarga-id" type="hidden"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Recarga</div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input readonly type="text" class="form-control recarga-nombres" placeholder="Nombres"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Saldo</label>
                            <input readonly type="text" class="form-control recarga-saldo" placeholder="Saldo"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta bancaria</label>
                            <select name="numero_cuenta" class="form-control recarga-cuenta-bancaria" placeholder="Banco">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Contacto</label>
                            <select name="numero_contacto" class="form-control recarga-contacto" placeholder="Tipo">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Monto de apuesta (S/)</label>
                            <input name="monto" type="text" class="recarga-monto form-control" placeholder="Monto"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-registrar-jugad2or" type="submit" class="btn btn-primary">Registrar</button>
                <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
            </div>
        </div> 
    </div>
    </form>
</div>
