<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::resource('api_jugadores', 'App\Http\Controllers\JugadorController', ['only' => ['show'] ]);
Route::get('api_apuestas/{id}', 'App\Http\Controllers\ApuestaController@listar')->name('apuestas.lista');
Route::post('api_apuestas/anulaciones/{id}', 'App\Http\Controllers\ApuestaController@anularApuesta')->name('anular.movimiento');

