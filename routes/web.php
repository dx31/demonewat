<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::post('jugadores', ['as'=> 'jugadores.store', 'uses' => 'App\Http\Controllers\JugadorController@store' ]);
Route::get('apuestas', ['as'=> 'apuestas.index', 'uses' => 'App\Http\Controllers\ApuestaController@index' ]);
Route::post('apuestas', ['as'=> 'apuestas.store', 'uses' => 'App\Http\Controllers\ApuestaController@store' ]);

Route::get('reportes', ['as'=> 'reportes.index', 'uses' => 'App\Http\Controllers\ReportesController@index' ]);


Route::group(['middleware' => 'auth'], function () {
    Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});
